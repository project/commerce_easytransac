<?php

namespace Drupal\commerce_easytransac;

use Drupal\address\Repository\CountryRepository;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;
use EasyTransac\Entities\Customer;

/**
 * EasyTransac service.
 *
 * Helpers, methods used globally in the module.
 */
class EasyTransac {

  /**
   * EasyTransac platform URL.
   */
  public const EASYTRANSAC_URL = 'https://www.easytransac.com';

  /**
   * Default currency.
   */
  public const EASYTRANSAC_DEFAULT_CURRENCY = 'EUR';

  /**
   * Minimum amount for multiple payments.
   */
  public const EASYTRANSAC_MULTIPLE_MIN_AMOUNT = 50;

  /**
   * Minimum authorization duration for payments.
   */
  public const EASYTRANSAC_MIN_AUTH_DURATION = 6;

  /**
   * Country repository service.
   *
   * @var \Drupal\address\Repository\CountryRepository
   */
  protected $coutryRepository;

  /**
   * Constructs a new EasyTransac service.
   *
   * @param \Drupal\address\Repository\CountryRepository $coutryRepository
   *   Country repository service.
   */
  public function __construct(CountryRepository $coutryRepository) {
    $this->coutryRepository = $coutryRepository;
  }

  /**
   * Check if this price is eligible for multiples payments.
   *
   * @param \Drupal\commerce_price\Price $price
   *   Price entity.
   *
   * @return bool
   *   TRUE if the order is eligible, FALSE otherwise.
   */
  public static function isEligibleForMultiplePayments(Price $price): bool {
    return $price->greaterThan(new Price((string) self::EASYTRANSAC_MULTIPLE_MIN_AMOUNT, $price->getCurrencyCode()));
  }

  /**
   * Calculate the first down payment price.
   *
   * @param \Drupal\commerce_price\Price $price
   *   Price entity.
   * @param int $count
   *   Number of down payments.
   *
   * @return \Drupal\commerce_price\Price
   *   First down payment price entity.
   */
  public static function downPaymentAmount(Price $price, int $count): Price {
    $amount = $price->getNumber();
    $down_payment = (int) floor($amount / $count);
    $first_amount = $down_payment + ($amount % $count);
    return new Price($first_amount, $price->getCurrencyCode());
  }

  /**
   * Calculate all down payment prices.
   *
   * @param \Drupal\commerce_price\Price $price
   *   Price entity.
   * @param int $count
   *   Number of down payments.
   *
   * @return array
   *   Down payment price entities.
   */
  public static function downPaymentAmounts(Price $price, int $count): array {
    $amount = $price->getNumber();
    $down_payment = (int) floor($amount / $count);
    $first_amount = $down_payment + ($amount % $count);

    return [
      new Price($first_amount, $price->getCurrencyCode()),
      new Price($down_payment, $price->getCurrencyCode()),
    ];
  }

  /**
   * Format the langcode for EasyTransac APIs.
   *
   * @param string $langcode
   *   Language code.
   *
   * @return string
   *   Language code in ISO 639-2 alpha-3.
   *
   * @see https://www.easytransac.com/fr/documentation#section/Parametre
   */
  public static function formatLanguage(string $langcode): string {
    $mapping = [
      'fr' => 'FRE',
      'en' => 'ENG',
    ];

    return strtoupper($mapping[$langcode] ?? $langcode);
  }

  /**
   * Get the commerce order billing address.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Commerce order entity.
   *
   * @return array|null
   *   Billing address data array.
   */
  public function getBillingAddress(OrderInterface $order): ?array {
    $billing_profile = $order->getBillingProfile();
    $billing_address = NULL;

    if ($billing_profile !== NULL) {
      $billing_address = $billing_profile->get('address')->first()->getValue();
    }

    return $billing_address;
  }

  /**
   * Get the commerce order billing country.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Commerce order entity.
   *
   * @return array|null
   *   Billing country in 3 letters code.
   */
  public function getBillingCountry(OrderInterface $order): string {
    $billing_address = $this->getBillingAddress($order);
    $billing_profile = $order->getBillingProfile();
    $billing_country = NULL;

    // Find the three letters billing country code.
    if ($billing_profile !== NULL
      && !empty($billing_address['country_code'])
      && $country = $this->coutryRepository->get($billing_address['country_code'])) {
      $billing_country = $country->getThreeLetterCode();
    }

    return $billing_country;
  }

  /**
   * Get EasyTransac API customer entity for this order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Commerce order entity.
   *
   * @return \EasyTransac\Entities\Customer
   *   EasyTransac API customer entity.
   */
  public function getCustomer(OrderInterface $order): Customer {
    $user = $order->getCustomer();
    $billing_address = $this->getBillingAddress($order);
    $billing_country = $this->getBillingCountry($order);

    $customer = (new Customer())
      ->setEmail(!empty($user->getEmail()) ? $user->getEmail() : $order->getEmail())
      ->setUid($user->id());

    if ($billing_address !== NULL) {
      $customer
        ->setFirstname($billing_address['given_name'])
        ->setLastname($billing_address['family_name'])
        ->setAddress($billing_address['address_line1'] . (!empty($billing_address['address_line2']) ? ', ' . $billing_address['address_line2'] : ''))
        ->setZipCode($billing_address['postal_code'])
        ->setCity($billing_address['locality'])
        ->setCompany($billing_address['organization'])
        ->setCountry($billing_country);
    }

    return $customer;
  }

}
