<?php

namespace Drupal\commerce_easytransac\Event;

use EasyTransac\Requests\Request;
use EasyTransac\Entities\Entity;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the event for altering the request before sending it to EasyTransac.
 *
 * @see \Drupal\commerce_easytransac\Event\EasyTransacEvents
 */
class EasyTransacRequestEvent extends Event {

  /**
   * The payment gateway configuration.
   *
   * @var array
   */
  protected $configuration;

  /**
   * The EasyTransac request object.
   *
   * @var \EasyTransac\Requests\Request
   */
  protected $request;

  /**
   * The EasyTransac entity object.
   *
   * @var \EasyTransac\Entities\Entity
   */
  protected $entity;

  /**
   * Additional contextual data.
   *
   * @var array
   */
  protected $context;

  /**
   * Constructs a new EasyTransacRequestEvent object.
   *
   * @param array $configuration
   *   The payment gateway configuration.
   * @param \EasyTransac\Requests\Request $request
   *   The request entity.
   * @param \EasyTransac\Entities\Entity $entity
   *   The entity to use for the request.
   * @param array $context
   *   Additional contextual data.
   */
  public function __construct(array $configuration, Request $request, Entity $entity, array $context) {
    $this->configuration = $configuration;
    $this->request = $request;
    $this->entity = $entity;
    $this->context = $context;
  }

  /**
   * Gets the payment gateway configuration.
   *
   * @return array
   *   The payment gateway configuration.
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * Gets the EasyTransac request object.
   *
   * @return \EasyTransac\Requests\Request
   *   The EasyTransac request object.
   */
  public function getRequest(): Request {
    return $this->request;
  }

  /**
   * Gets the EasyTransac entity object.
   *
   * @return \EasyTransac\Entities\Entity
   *   The EasyTransac entity.
   */
  public function getEntity(): Entity {
    return $this->entity;
  }

  /**
   * Gets the additional contextual data.
   *
   * @return array
   *   The additional contextual data.
   */
  public function getContext(): array {
    return $this->context;
  }

}
