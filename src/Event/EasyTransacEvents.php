<?php

namespace Drupal\commerce_easytransac\Event;

/**
 * Class EasyTransacEvents.
 *
 * Events related to EasyTransac commerce module.
 */
final class EasyTransacEvents {

  /**
   * Name of the event fired before a payment creation request is sent.
   *
   * @Event
   *
   * @see \Drupal\commerce_easytransac\Event\EasyTransacRequestEvent
   */
  const PAYMENT_CREATE_REQUEST = 'commerce_easytransac.payment_create_request';

  /**
   * Name of the event fired before a payment capture request is sent.
   *
   * @Event
   *
   * @see \Drupal\commerce_easytransac\Event\EasyTransacRequestEvent
   */
  const PAYMENT_CAPTURE_REQUEST = 'commerce_easytransac.payment_capture_request';

  /**
   * Name of the event fired before a payment void request is sent.
   *
   * @Event
   *
   * @see \Drupal\commerce_easytransac\Event\EasyTransacRequestEvent
   */
  const PAYMENT_VOID_REQUEST = 'commerce_easytransac.payment_void_request';

  /**
   * Name of the event fired before a payment refund request is sent.
   *
   * @Event
   *
   * @see \Drupal\commerce_easytransac\Event\EasyTransacRequestEvent
   */
  const PAYMENT_REFUND_REQUEST = 'commerce_easytransac.payment_refund_request';

}
