<?php

namespace Drupal\commerce_easytransac\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the payment type for EasyTransac PayByBank.
 *
 * @CommercePaymentType(
 *   id = "payment_easytransac_paybybank",
 *   label = @Translation("EasyTransac PayByBank"),
 *   workflow = "payment_easytransac"
 * )
 */
class PaymentPayByBank extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
