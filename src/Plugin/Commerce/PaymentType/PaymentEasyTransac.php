<?php

namespace Drupal\commerce_easytransac\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the payment type for EasyTransac.
 *
 * @CommercePaymentType(
 *   id = "payment_easytransac",
 *   label = @Translation("EasyTransac"),
 *   workflow = "payment_easytransac"
 * )
 */
class PaymentEasyTransac extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];

    $fields['request_id'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Request identifier'))
      ->setDescription(t('EasyTransac API Payment page request identifier.'))
      ->setRequired(FALSE);

    return $fields;
  }

}
