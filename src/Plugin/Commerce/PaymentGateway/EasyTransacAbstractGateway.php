<?php

namespace Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_easytransac\ApplicationInterface;
use Drupal\commerce_easytransac\EasyTransac as EasyTransacHelper;
use Drupal\commerce_easytransac\Environment;
use Drupal\commerce_easytransac\Event\EasyTransacEvents;
use Drupal\commerce_easytransac\Event\EasyTransacRequestEvent;
use Drupal\commerce_easytransac\PluginForm\EasyTransac\PaymentGetStatusForm;
use Drupal\commerce_easytransac\PluginForm\EasyTransac\PaymentSyncForm;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use EasyTransac\Core\PaymentNotification;
use EasyTransac\Core\Services;
use EasyTransac\Entities\Cancellation;
use EasyTransac\Entities\DoneTransaction;
use EasyTransac\Entities\Entity;
use EasyTransac\Entities\Notification;
use EasyTransac\Entities\PaymentCapture;
use EasyTransac\Entities\PaymentStatus;
use EasyTransac\Entities\Refund;
use EasyTransac\Requests\Cancellation as CancellationRequest;
use EasyTransac\Requests\Capture;
use EasyTransac\Requests\PaymentRefund;
use EasyTransac\Requests\PaymentStatus as PaymentStatusRequest;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides the EasyTransac payment gateway common code.
 */
abstract class EasyTransacAbstractGateway extends OffsitePaymentGatewayBase implements MainEasyTransacInterface {

  /**
   * The EasyTransac application.
   *
   * @var \Drupal\commerce_easytransac\Application
   */
  protected $application;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('commerce_easytransac.application'),
      $container->get('event_dispatcher'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, MinorUnitsConverterInterface $minor_units_converter, ApplicationInterface $application, EventDispatcherInterface $event_dispatcher, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);

    $this->application = $application;
    $this->eventDispatcher = $event_dispatcher;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_key' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['mode']['#access'] = FALSE; // Disable mode selection.

    $language = $this->languageManager->getCurrentLanguage();
    $form['api_configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('EasyTransac API'),
      '#parents' => $form['#parents'],
    ];

    // phpcs:disable
    $documentationUrl = EasyTransacHelper::EASYTRANSAC_URL . '/' . $language->getId() . '/documentation';
    $form['api_configuration']['help'] = [
      '#markup' => '<p>' . $this->t('You are configuring an EasyTransac application, please follow the instructions below to set your API key.') . '<br/>' .
      $this->t('Get more information on the <a href="@link_api">API documentation page</a> or directly on the <a href="@link_sdk">PHP SDK readme file</a>.', ['@link_api' => $documentationUrl, '@link_sdk' => 'https://github.com/easytransac/easytransac-sdk-php/blob/master/README.md']) . '</p>',
      '#weight' => -50,
    ];

    $form['api_configuration']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('Provide the API key of your EasyTransac account.') . '<br/>' . $this->t('If you don\'t have an account yet, register at <a href="@link">EasyTransac website</a> and follow "<a href="@link_inst">Get an API key</a>" instructions.', ['@link' => EasyTransacHelper::EASYTRANSAC_URL, '@link_inst' => $documentationUrl]),
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];

    // Use the same notification URl as the EasyTransac regular gateway.
    // Anyway, only one notification URL can be configured on EasyTransac app.
    $form['api_configuration']['notification_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Notification URL'),
      '#description' => $this->t('URL where payment notifications will be sent. Copy/paste it to your EasyTransac application.') . '<br/>'
      . $this->t('If you are using HTTPS but the notification URL below does not show in HTTPS, then you have to configure Drupal properly.') . '<br/>'
      . $this->t('Common cases are found when Drupal needs to be configured when running behind a <a href="@link">reverse proxy</a>.', ['@link' => 'https://www.drupal.org/node/425990']),
      '#default_value' => Url::fromRoute('commerce_payment.notify', [
        'commerce_payment_gateway' => 'easytransac',
      ], ['absolute' => TRUE])->toString(),
      '#disabled' => TRUE,
    ];
    // phpcs:enable

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_key'] = $values['api_key'];
    }
  }

  /**
   * Gets the default payment gateway forms.
   *
   * @return array
   *   A list of plugin form classes keyed by operation.
   */
  protected function getDefaultForms() {
    $default_forms = parent::getDefaultForms();

    $default_forms['sync-payment'] = PaymentSyncForm::class;
    $default_forms['status-payment'] = PaymentGetStatusForm::class;

    return $default_forms;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentOperations(PaymentInterface $payment) {
    $operations = parent::buildPaymentOperations($payment);

    $operations['sync'] = [
      'title' => $this->t('Synchronize'),
      'page_title' => $this->t('Synchronize the payment with the distant payment'),
      'plugin_form' => 'sync-payment',
      'access' => $this->canSyncPayment($payment),
    ];

    $operations['status'] = [
      'title' => $this->t('Status'),
      'page_title' => $this->t('Get the status of the payment'),
      'plugin_form' => 'status-payment',
      'access' => $this->canGetStatusOfPayment($payment),
    ];

    return $operations;
  }

  /**
   * {@inheritDoc}
   */
  public function canSyncPayment(PaymentInterface $payment): bool {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function syncPayment(PaymentInterface $payment): void {
    $this->syncExistingPayment($payment, $this->getStatusOfPayment($payment));
  }

  /**
   * {@inheritDoc}
   */
  public function canGetStatusOfPayment(PaymentInterface $payment): bool {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function getStatusOfPayment(PaymentInterface $payment): DoneTransaction {
    $transaction = (new PaymentStatus())
      ->setLanguage($this->getApplication()->getLanguage());

    if ($payment->getRemoteId() !== NULL) {
      $transaction->setTid($payment->getRemoteId());
    }

    if ($payment->hasField('request_id')
      && !$payment->get('request_id')->isEmpty()) {
      $transaction->setRequestId($payment->get('request_id')->first()->getValue()['value']);
    }

    $this->getClient();
    $response = (new PaymentStatusRequest())->execute($transaction);
    if (!$response->isSuccess()) {
      throw new PaymentGatewayException($response->getErrorMessage());
    }

    return $response->getContent();
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);

    try {
      // Try to parse the EasyTransac notification response
      // and verify signature.
      $this->getClient();
      $response = PaymentNotification::getContent($request->request->all(), $this->getApiKey());
    }
    catch (\Exception $exception) {
      $this->processException($exception, TRUE);
    }

    // Make sure the request order identifier is the same
    // as the one given back.
    if ($order->id() !== $response->getOrderId()) {
      throw new PaymentGatewayException('EasyTransac request order identifier does not match.');
    }

    $this->matchCustomer($order, $response);
    $this->updateUserRemoteIdentifier($order, $response);

    $paymentMethod = NULL;
    if (!empty($alias = $response->getAlias())) {
      $paymentMethod = $this->updateUserPaymentMethods($order, $alias);
    }

    $this->processTransactionPayment($order, $response, $paymentMethod);
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);

    // Clean the stored data on the order.
    $order->unsetData('easytransac_request_identifier');

    $order->save();
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    parent::onNotify($request);

    // Bypass any non-POST requests.
    if ($request->getMethod() !== 'POST') {
      throw new NotFoundHttpException();
    }

    try {
      // Try to parse the EasyTransac notification response
      // and verify signature.
      $this->getClient();
      $response = PaymentNotification::getContent($request->request->all(), $this->getApiKey());
    }
    catch (\Exception $exception) {
      $this->processException($exception, FALSE);
    }

    // Load the transaction order.
    $order_id = $response->getOrderId();
    $order = Order::load($order_id);
    if ($order === NULL) {
      throw new PaymentGatewayException('Order not found.');
    }

    $this->matchCustomer($order, $response);
    $this->updateUserRemoteIdentifier($order, $response);

    $paymentMethod = NULL;
    if (!empty($alias = $response->getAlias())) {
      $paymentMethod = $this->updateUserPaymentMethods($order, $alias);
    }

    $this->processTransactionPayment($order, $response, $paymentMethod);
  }

  /**
   * Process an EasyTransac SDK exception.
   *
   * @param \Exception $exception
   *   Exception entity to process.
   * @param bool $logError
   *   Log the error as well as return an exception.
   */
  protected function processException(\Exception $exception, bool $logError): void {
    // Gives more explanation about some internal error messages.
    $message = $exception->getMessage();
    if ($message === '$data is empty') {
      $message = 'No POST data was sent back by EasyTransac, this usually comes from an undesired 302 redirection.';
      $message .= 'If you use HTTPS, make sure that your webserver is properly generating HTTPS urls.';
    }

    if ($logError) {
      \Drupal::logger('commerce_easytransac')
        ->error('Notification request error: ' . $message);
    }

    throw new PaymentGatewayException($message);
  }

  /**
   * Match customer identifier.
   *
   * Make sure the payment page user identifier
   * is the same as the one given back.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Commerce order entity.
   * @param \EasyTransac\Entities\Notification|\EasyTransac\Entities\DoneTransaction $response
   *   EasyTransac API response entity.
   */
  protected function matchCustomer(OrderInterface $order, Entity $response): void {
    if (!($response instanceof Notification || $response instanceof DoneTransaction)) {
      throw new PaymentGatewayException('matchCustomer expects a Notification or DoneTransaction instance.');
    }

    if (empty($response->getUid())) {
      if (((int) $order->getCustomerId()) !== 0) {
        throw new PaymentGatewayException('EasyTransac payment page user is anonymous whereas the order one is not.');
      }
    }
    elseif ($order->getCustomerId() !== $response->getUid()) {
      throw new PaymentGatewayException('EasyTransac payment page user identifier "' . $response->getUid() . '" does not match the order one "' . $order->getCustomerId() . '"');
    }
  }

  /**
   * Update the EasyTransac remote identifier for the user entity.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Commerce order entity.
   * @param \EasyTransac\Entities\Notification|\EasyTransac\Entities\DoneTransaction $response
   *   EasyTransac API response entity.
   */
  protected function updateUserRemoteIdentifier(OrderInterface $order, Entity $response): void {
    if (!($response instanceof Notification || $response instanceof DoneTransaction)) {
      throw new PaymentGatewayException('updateUserRemoteIdentifier expects a Notification or DoneTransaction instance.');
    }

    // Update the EasyTransac remote identifier for the user entity.
    $user = $order->getCustomer();
    $new_customer_identifier = $response->getClient()->getId();
    if (empty($current_customer_identifier = $this->getRemoteCustomerId($user))
      || $current_customer_identifier !== $new_customer_identifier) {
      $this->setRemoteCustomerId($user, $new_customer_identifier);
      $user->save();
    }
  }

  /**
   * Create/update the order payment after transaction.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Commerce order entity.
   * @param \EasyTransac\Entities\Notification|\EasyTransac\Entities\DoneTransaction $response
   *   EasyTransac API response entity.
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface|null $payment_method
   *   Commerce payment method entity, or NULl if none to be associated
   *   with the created/updated payment.
   */
  protected function processTransactionPayment(OrderInterface $order, Entity $response, ?PaymentMethodInterface $payment_method = NULL): void {
    if (!($response instanceof Notification || $response instanceof DoneTransaction)) {
      throw new PaymentGatewayException('processTransactionPayment expects a Notification or DoneTransaction instance.');
    }

    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $order_id = $response->getOrderId();
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $query = $payment_storage->getQuery();
    $payment_ids = $query
      ->condition('order_id', $order_id)
      ->condition($query->orConditionGroup()
        ->condition('remote_id', $response->getTid())
        ->condition('request_id', $response->getRequestId()))
      ->accessCheck(FALSE)
      ->sort('payment_id', 'DESC')
      ->range(0, 1)
      ->execute();

    if (!empty($payment_ids)) {
      // Update the existing payment transaction for this order.
      $payment = $payment_storage->load(reset($payment_ids));
      if (!($payment instanceof PaymentInterface)) {
        throw new PaymentGatewayException('Can not load the payment entity.');
      }

      $this->syncExistingPayment($payment, $response);
    }
    else {
      $this->syncNewPayment($order, $response, $payment_method);
    }
  }

  /**
   * Synchronize existing payments with drupal database.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   Commerce payment entity.
   * @param \EasyTransac\Entities\Notification|\EasyTransac\Entities\DoneTransaction $response
   *   EasyTransac API response entity.
   */
  protected function syncExistingPayment(PaymentInterface $payment, Entity $response): void {
    if (!($response instanceof Notification || $response instanceof DoneTransaction)) {
      throw new PaymentGatewayException('syncNewPayment expects a Notification or DoneTransaction instance.');
    }

    // Synchronize the payment gateway, if different.
    if ($payment->getPaymentGatewayId() !== $this->parentEntity->id()) {
      $payment->set('payment_gateway', $this->parentEntity->id());
      $payment->save();
    }

    // Synchronize the remote identifier, if different.
    if ($payment->getRemoteId() !== $response->getTid()) {
      $payment->setRemoteId($response->getTid());
      $payment->save();
    }

    // Synchronize the remote state, if different.
    if ($payment->getRemoteState() !== $response->getStatus()) {
      $payment->setRemoteState($response->getStatus());
      $payment->save();
    }

    // Process the payment according to the status.
    switch ($response->getStatus()) {
      case 'captured':
        $amount = Price::fromArray([
          'number' => $response->getAmountPreAuth() ?? $response->getAmount(),
          'currency_code' => $response->getCurrency() ?? EasyTransacHelper::EASYTRANSAC_DEFAULT_CURRENCY,
        ]);

        if ($payment->getState()->getId() !== 'completed' ||
          $amount->lessThan($payment->getAmount())) {
          $payment->setAmount($amount);
          $payment->setState('completed');
          $payment->save();
        }
        break;

      case 'pending':
        if ($payment->getState()->getId() !== 'pending') {
          $amount = Price::fromArray([
            'number' => $response->getAmount(),
            'currency_code' => $response->getCurrency() ?? EasyTransacHelper::EASYTRANSAC_DEFAULT_CURRENCY,
          ]);

          $payment->setAmount($amount);
          $payment->setState('pending');
          $payment->save();
        }
        break;

      case 'refunded':
        $amount = Price::fromArray([
          'number' => $response->getAmountRefund(),
          'currency_code' => $response->getCurrency() ?? EasyTransacHelper::EASYTRANSAC_DEFAULT_CURRENCY,
        ]);

        if ($amount->lessThan($payment->getAmount())) {
          $payment->setState('partially_refunded');
        }
        else {
          $payment->setState('refunded');
        }

        if (!$payment->getRefundedAmount() ||
          !$payment->getRefundedAmount()->equals($amount)) {
          $payment->setRefundedAmount($amount);
          $payment->save();
        }
        break;

      case 'authorized':
        if ($payment->getState()->getId() !== 'authorization') {
          $amount = Price::fromArray([
            'number' => $response->getAmountPreAuth() ?? $response->getAmount(),
            'currency_code' => $response->getCurrency() ?? EasyTransacHelper::EASYTRANSAC_DEFAULT_CURRENCY,
          ]);

          $payment->setAmount($amount);
          $payment->setState('authorization');
          $payment->save();
        }
        break;

      case 'failed':
        if (!in_array($payment->getState()->getId(), [
          'failed',
          'authorization_voided',
        ], TRUE)) {
          $payment->setState('failed');
          $payment->save();
        }
        break;

      default:
        throw new PaymentGatewayException('Unknown EasyTransac payment notification status received : ' . $response->getStatus());
    }
  }

  /**
   * Synchronize new payments with drupal database.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Commerce order entity.
   * @param \EasyTransac\Entities\Notification|\EasyTransac\Entities\DoneTransaction $response
   *   EasyTransac API response entity.
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface|null $payment_method
   *   Commerce payment method entity, or NULl if none to be associated
   *   with the created/updated payment.
   */
  protected function syncNewPayment(OrderInterface $order, Entity $response, ?PaymentMethodInterface $payment_method = NULL): void {
    if (!($response instanceof Notification || $response instanceof DoneTransaction)) {
      throw new PaymentGatewayException('syncNewPayment expects a Notification or DoneTransaction instance.');
    }

    switch ($response->getStatus()) {
      case 'captured':
        $payment_state = 'completed';
        break;

      case 'pending':
        $payment_state = 'new';
        break;

      case 'refunded':
        // partially_refunded.
        $payment_state = 'refunded';
        break;

      case 'authorized':
        // authorization_voided
        // authorization_expired.
        $payment_state = 'authorization';
        break;

      case 'failed':
        // Do not save any payment, simply ignore.
        return;

      default:
        throw new PaymentGatewayException('Unknown EasyTransac payment notification status received : ' . $response->getStatus());
    }

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => $payment_state,
      'amount' => new Price($response->getAmount(), $response->getCurrency() ?? EasyTransacHelper::EASYTRANSAC_DEFAULT_CURRENCY),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order->id(),
      'remote_id' => $response->getTid(),
      'remote_state' => $response->getStatus(),
      'payment_method' => $payment_method,
      'test' => $response->getTest() === 'yes' ? 'test' : 'live',
    ]);

    // Add the authorization expiration time if pre-authorization.
    // PayByBank operations seems to be only pre-authorization first as well.
    if ($response->getPreAuth() === 'yes' || $response->getOperationType() === 'paybybank') {
      $duration = !empty($this->configuration['authorization_duration']) ? $this->configuration['authorization_duration'] : EasyTransacHelper::EASYTRANSAC_MIN_AUTH_DURATION;
      $payment->setExpiresTime(time() + ($duration * 24 * 60 * 60));
    }

    // Map the order data to the payment fields.
    // In case we are dealing with a payment page notification.
    if ($response->getOperationType() === 'payment' && !empty($response->getRequestId())) {
      $payment->set('request_id', $response->getRequestId());
    }

    $payment->save();
  }

  /**
   * {@inheritDoc}
   */
  public function getSupportedModes() {
    return [
      Environment::TEST => $this->t('Demo'),
      Environment::PRODUCTION => $this->t('Real'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMode() {
    if (($env = $this->getEnvironment()) !== NULL) {
      return $env;
    }

    return parent::getMode();
  }

  /**
   * {@inheritdoc}
   */
  public function canCapturePayment(PaymentInterface $payment) {
    // EasyTransac API does not support cature on PayByBank payments.
    return parent::canCapturePayment($payment)
      && $payment->bundle() !== 'payment_easytransac_paybybank';
  }

  /**
   * {@inheritdoc}
   */
  public function canVoidPayment(PaymentInterface $payment) {
    // EasyTransac API does not support void on PayByBank payments.
    return parent::canVoidPayment($payment)
      && $payment->bundle() !== 'payment_easytransac_paybybank';
  }

  /**
   * {@inheritdoc}
   */
  public function canRefundPayment(PaymentInterface $payment) {
    // EasyTransac API only supports one refund on a payment.
    return $payment->getState()->getId() === 'completed';
  }

  /**
   * Attempt to validate payment information according to a payment state.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment to validate.
   * @param string|null $payment_state
   *   The payment state to validate the payment for.
   */
  protected function validatePayment(PaymentInterface $payment, $payment_state = 'new') {
    $this->assertPaymentState($payment, [$payment_state]);

    switch ($payment_state) {
      case 'new':
        $payment_method = $payment->getPaymentMethod();
        if ($payment_method !== NULL && $payment_method->isExpired()) {
          throw new HardDeclineException('The provided payment method has expired.');
        }
        break;

      case 'authorization':
        if ($payment->isExpired()) {
          throw new \InvalidArgumentException('Authorizations are guaranteed for up to 29 days.');
        }
        if (empty($payment->getRemoteId())) {
          throw new \InvalidArgumentException('Could not retrieve the transaction ID.');
        }
        break;

      case 'completed':
        if (empty($payment->getRemoteId())) {
          throw new \InvalidArgumentException('Could not retrieve the transaction ID.');
        }
        break;

      default;
        // Nothing to do.
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->validatePayment($payment, 'authorization');

    $remoteId = $payment->getRemoteId();
    if (empty($remoteId)) {
      throw new PaymentGatewayException('Remote authorization ID could not be determined.');
    }

    $this->getClient();
    $paymentCapture = (new PaymentCapture())
      ->setTid($remoteId)
      ->setLanguage($this->getApplication()->getLanguage());

    if ($amount !== NULL && ($payment->getAmount() === NULL || !$payment->getAmount()->equals($amount))) {
      $paymentCapture->setAmount($this->minorUnitsConverter->toMinorUnits($amount));
    }

    $request = new Capture();
    $event = new EasyTransacRequestEvent($this->configuration, $request, $paymentCapture, ['payment' => $payment]);
    $this->eventDispatcher->dispatch($event, EasyTransacEvents::PAYMENT_CAPTURE_REQUEST);
    $response = $event->getRequest()->execute($event->getEntity());
    if (!$response->isSuccess()) {
      throw new PaymentGatewayException('Count not capture payment. Message: ' . $response->getErrorMessage(), $response->getErrorCode());
    }

    // Update payment entity.
    if ($payment->getState()->getId() !== 'completed' ||
      $amount->lessThan($payment->getAmount())) {
      $payment->setAmount($amount);
      $payment->setState('completed');
      $payment->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->validatePayment($payment, 'authorization');

    $remoteId = $payment->getRemoteId();
    if (empty($remoteId)) {
      throw new PaymentGatewayException('Remote authorization ID could not be determined.');
    }

    $this->getClient();
    $cancellation = (new Cancellation())
      ->setTid($remoteId)
      ->setLanguage($this->getApplication()->getLanguage());

    $request = new CancellationRequest();
    $event = new EasyTransacRequestEvent($this->configuration, $request, $cancellation, ['payment' => $payment]);
    $this->eventDispatcher->dispatch($event, EasyTransacEvents::PAYMENT_VOID_REQUEST);
    $response = $event->getRequest()->execute($event->getEntity());
    if (!$response->isSuccess()) {
      throw new PaymentGatewayException('Count not void payment. Message: ' . $response->getErrorMessage(), $response->getErrorCode());
    }

    // Update payment entity.
    if ($payment->getState()->getId() !== 'authorization_voided') {
      $payment->setState('authorization_voided');
      $payment->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    if (empty($payment->getRemoteId())) {
      throw new \InvalidArgumentException('Could not retrieve the transaction ID.');
    }

    $this->getClient();
    $user = $this->getApplication()->getCurrentUser();

    $refund = (new Refund())
      ->setTid($payment->getRemoteId())
      ->setReason((string) $this->t('Refund made through Drupal Commerce backoffice by user @user.', ['@user' => $user->id()]))
      ->setLanguage($this->getApplication()->getLanguage());

    if ($amount !== NULL && ($payment->getAmount() === NULL || !$payment->getAmount()->equals($amount))) {
      $refund->setAmount($this->minorUnitsConverter->toMinorUnits($amount));
    }

    $request = new PaymentRefund();
    $event = new EasyTransacRequestEvent($this->configuration, $request, $refund, ['payment' => $payment]);
    $this->eventDispatcher->dispatch($event, EasyTransacEvents::PAYMENT_REFUND_REQUEST);
    $response = $event->getRequest()->execute($event->getEntity());
    if (!$response->isSuccess()) {
      throw new PaymentGatewayException('Count not refund payment. Message: ' . $response->getErrorMessage(), $response->getErrorCode());
    }

    // Update payment entity.
    if ($amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    if (!$payment->getRefundedAmount() ||
      !$payment->getRefundedAmount()->equals($amount)) {
      $payment->setRefundedAmount($amount);
      $payment->save();
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getApiKey(): ?string {
    return $this->configuration['api_key'];
  }

  /**
   * {@inheritDoc}
   */
  public function getApplication(): ApplicationInterface {
    return $this->application;
  }

  /**
   * {@inheritdoc}
   */
  public function getClient(): Services {
    return $this->getApplication()->initClient($this->getApiKey());
  }

  /**
   * {@inheritDoc}
   */
  public function getEnvironment(): ?string {
    $env = NULL;

    if (!empty($this->configuration['api_key'])) {
      if (str_starts_with($this->configuration['api_key'], 'et_test_')) {
        $env = Environment::TEST;
      }
      elseif (str_starts_with($this->configuration['api_key'], 'et_live_')) {
        $env = Environment::PRODUCTION;
      }
    }

    return $env;
  }

  /**
   * {@inheritDoc}
   */
  public function isLive(): bool {
    return $this->getEnvironment() === Environment::PRODUCTION;
  }

  /**
   * Check if the user already has this payment method.
   *
   * This function also updates the payment method when necessary.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Commerce order entity.
   * @param string $card_alias
   *   EasyTransac card alias.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentMethodInterface
   *   Payment method entity, or NULL if none updated/created.
   */
  abstract protected function updateUserPaymentMethods(OrderInterface $order, string $card_alias): ?PaymentMethodInterface;

}
