<?php

namespace Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Supports multiple payment interface.
 *
 * Defines the interface for EasyTransac gateways
 * which support multiple payments.
 */
interface SupportsMultiplePaymentInterface {

  /**
   * Check if the order is eligible for EasyTransac multiple payments.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Commerce order entity.
   *
   * @return bool
   *   TRUE if the order is eligible, FALSE otherwise.
   */
  public function isEligibleForMultiplePayments(OrderInterface $order): bool;

  /**
   * Get the order EasyTransac multiple payments count.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Commerce order entity.
   *
   * @return int
   *   Number of payments or NULl if multiple payments not available.
   */
  public function getMultiplePaymentsCount(OrderInterface $order): ?int;

  /**
   * Check if EasyTransac multiple payments are enabled.
   *
   * @return bool
   *   TRUE if multiple payments are enabled, FALSE otherwise.
   */
  public function multiplePaymentEnabled(): bool;

  /**
   * Get the multiple payments options which are available.
   *
   * @return int[]
   *   An array of multiple payment options.
   */
  public function multiplePaymentOptions(): array;

}
