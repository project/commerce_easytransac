<?php

namespace Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway;

/**
 * Provides the interface for the EasyTransac PayByBank payment gateway.
 */
interface PayByBankInterface extends MainEasyTransacInterface {

}
