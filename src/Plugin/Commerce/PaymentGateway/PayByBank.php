<?php

namespace Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the EasyTransac PayByBank payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "easytransac_paybybank",
 *   label = "EasyTransac PayByBank",
 *   display_label = "Pay By Bank",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_easytransac\PluginForm\EasyTransac\PayByBankOffsiteForm",
 *     "add-payment" = "Drupal\commerce_easytransac\PluginForm\EasyTransac\PayByBankPaymentAddForm"
 *   },
 *   payment_type = "payment_easytransac_paybybank"
 * )
 */
class PayByBank extends EasyTransacAbstractGateway implements PayByBankInterface {

  /**
   * {@inheritDoc}
   */
  protected function updateUserPaymentMethods(OrderInterface $order, string $card_alias): ?PaymentMethodInterface {
    // This gateway does not support payment methods.
    return NULL;
  }

}
