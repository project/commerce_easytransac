<?php

namespace Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_easytransac\Event\EasyTransacEvents;
use Drupal\commerce_easytransac\Event\EasyTransacRequestEvent;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\Core\Form\FormStateInterface;
use EasyTransac\Entities\CreditCard;
use EasyTransac\Entities\OneClickTransaction;
use EasyTransac\Requests\CreditCardInfo;
use EasyTransac\Requests\OneClickPayment;
use Drupal\commerce_easytransac\EasyTransac as EasyTransacHelper;

/**
 * Provides the EasyTransac payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "easytransac",
 *   label = "EasyTransac",
 *   display_label = "EasyTransac",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_easytransac\PluginForm\EasyTransac\PaymentOffsiteForm",
 *     "add-payment" = "Drupal\commerce_easytransac\PluginForm\EasyTransac\PaymentAddForm",
 *     "add-payment-method" = "Drupal\commerce_easytransac\PluginForm\EasyTransac\PaymentMethodAddForm"
 *   },
 *   payment_type = "payment_easytransac",
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex",
 *     "maestro",
 *     "mastercard",
 *     "visa",
 *   },
 * )
 */
class EasyTransac extends EasyTransacAbstractGateway implements EasyTransacInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'multiple_payment' => TRUE,
      'multiple_payment_settings' => [3],
      'oneclick_payment' => TRUE,
      'authorization_duration' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['oneclick_payment_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('OneClick payments'),
      '#parents' => $form['#parents'],
    ];

    $form['oneclick_payment_wrapper']['oneclick_payment'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow OneClick payments'),
      '#description' => $this->t('OneClick payments use customer previously used credit card for easier checkout.'),
      '#default_value' => $this->configuration['oneclick_payment'],
    ];

    $form['multiple_payment_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Multiple payments'),
      '#parents' => $form['#parents'],
    ];

    $form['multiple_payment_wrapper']['multiple_payment'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow multiple payments'),
      '#description' => $this->t('Option available on demand. Available for payments starting at 50 EUR.'),
      '#default_value' => $this->configuration['multiple_payment'],
    ];

    $options = [];
    for ($i = 2; $i <= 12; $i++) {
      $options[$i] = $this->t('Allow payment in %i times', ['%i' => $i]);
    }

    $form['multiple_payment_wrapper']['multiple_payment_settings'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Multiple payments settings'),
      '#options' => $options,
      '#default_value' => $this->configuration['multiple_payment_settings'],
      '#states' => [
        'visible' => [
          'input[name="configuration[easytransac][multiple_payment]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['authorization_duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Preauthorization duration'),
      '#description' => $this->t('When using the "Authorize" transaction type, defines the duration of the pre-authorization, in days (1 to 30)'),
      '#default_value' => $this->configuration['authorization_duration'],
      '#min' => 1,
      '#max' => 30,
      '#step' => 1,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['oneclick_payment'] = $values['oneclick_payment'];
      $this->configuration['multiple_payment'] = $values['multiple_payment'];

      $multiple_payment_settings = [];
      foreach ($values['multiple_payment_settings'] as $key => $val) {
        if ($key === ((int) $val)) {
          $multiple_payment_settings[] = $key;
        }
      }

      $this->configuration['multiple_payment_settings'] = $multiple_payment_settings;
      $this->configuration['authorization_duration'] = $values['authorization_duration'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->validatePayment($payment, 'new');

    $order = $payment->getOrder();
    $customer = $order->getCustomer();
    $payment_method_alias = $payment->getPaymentMethod()->getRemoteId();
    if (empty($payment_method_alias)) {
      throw new PaymentGatewayException('EasyTransac payment method alias could not be determined.');
    }

    $customer_remote_id = $this->getRemoteCustomerId($customer);
    if (empty($customer_remote_id)) {
      throw new PaymentGatewayException('EasyTransac customer remote id could not be determined.');
    }

    $this->getClient();
    $minor_units_converter = \Drupal::service('commerce_price.minor_units_converter');
    $transaction = (new OneClickTransaction())
      ->setAmount($minor_units_converter->toMinorUnits($payment->getAmount()))
      ->setOrderId($order->id())
      ->setUid($order->getCustomerId())
      ->setClientIp($order->getIpAddress())
      ->setClientId($customer_remote_id)
      ->setAlias($payment_method_alias)
      ->setPreAuth(FALSE)
      ->setSecure(TRUE)
      ->setLanguage($this->getApplication()->getLanguage());

    // Only authorization will be done, no capture yet.
    if ($capture === FALSE) {
      $transaction
        ->setPreAuth(TRUE)
        ->setPreAuthDuration(!empty($this->configuration['authorization_duration']) ? $this->configuration['authorization_duration'] : 6);
    }

    // Multiple payments.
    if ($this->isEligibleForMultiplePayments($order)) {
      $payments_count = $this->getMultiplePaymentsCount($order);
      $transaction
        ->setMultiplePayments('yes')
        ->setMultiplePaymentsRepeat($payments_count)
        ->setDownPayment($minor_units_converter->toMinorUnits(EasyTransacHelper::downPaymentAmount($order->getTotalPrice(), $payments_count)));
    }

    $request = new OneClickPayment();
    $event = new EasyTransacRequestEvent($this->configuration, $request, $transaction, ['payment' => $payment]);
    $this->eventDispatcher->dispatch($event, EasyTransacEvents::PAYMENT_CREATE_REQUEST);
    $response = $event->getRequest()->execute($event->getEntity());
    if (!$response->isSuccess()) {
      throw new PaymentGatewayException('Could not charge the payment method. Message: ' . $response->getErrorMessage(), $response->getErrorCode());
    }

    /** @var \EasyTransac\Entities\DoneTransaction $done_response */
    $done_response = $response->getContent();
    $this->matchCustomer($order, $done_response);
    $this->updateUserRemoteIdentifier($order, $done_response);

    $paymentMethod = NULL;
    if (!empty($alias = $done_response->getAlias())) {
      $paymentMethod = $this->updateUserPaymentMethods($order, $alias);
    }

    $this->processTransactionPayment($order, $done_response, $paymentMethod);

    // Make sure the payment did not fail, otherwise we should not consider
    // the order as finished and redirect user to error step instead.
    if ($done_response->getStatus() === 'failed') {
      $error = $done_response->getError();
      if (!empty($done_response->getAdditionalError())) {
        $error .= ' (' . implode(', ', $done_response->getAdditionalError()) . ')';
      }

      throw new PaymentGatewayException($error);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // There are no remote records stored so delete the local entity only.
    $payment_method->delete();
  }

  /**
   * {@inheritDoc}
   */
  protected function updateUserPaymentMethods(OrderInterface $order, string $card_alias): ?PaymentMethodInterface {
    if (!$this->oneClickPaymentEnabled()) {
      return NULL;
    }

    $user = $order->getCustomer();
    if (!$user->isAuthenticated()) {
      return NULL;
    }

    $payment_method = NULL;
    $card = (new CreditCard())->setAlias($card_alias);
    $card_response = (new CreditCardInfo())->execute($card);
    if ($card_response->isSuccess()) {
      $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
      $billing_countries = $order->getStore()->getBillingCountries();

      $payment_method_exists = FALSE;
      $payment_methods = $payment_method_storage->loadReusable($user, $this->parentEntity, $billing_countries);
      foreach ($payment_methods as $payment_method) {
        /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
        if ($payment_method->getRemoteId() === $card_response->getContent()->getAlias()) {
          $payment_method_exists = TRUE;
        }
      }

      // Create the new payment method.
      if (!$payment_method_exists) {
        $payment_method = PaymentMethod::create([
          'type' => 'credit_card',
          'uid' => $user->id(),
          'payment_gateway' => $this->getPluginId(),
          'payment_gateway_mode' => $this->getMode(),
          'billing_profile' => $order->getBillingProfile(),
          'reusable' => TRUE,
          'remote_id' => $card_response->getContent()->getAlias(),
          'card_type' => strtolower($card_response->getContent()->getCardType()),
          'card_number' => substr($card_response->getContent()->getCardBin(), -4),
          'card_exp_year' => $card_response->getContent()->getCardYear(),
          'card_exp_month' => $card_response->getContent()->getCardMonth(),
          'expires' => strtotime($card_response->getContent()->getCardYear() . '/' . $card_response->getContent()->getCardMonth() . '/01'),
        ]);

        $payment_method->save();

        \Drupal::logger('commerce_easytransac')
          ->info('New payment method (' . $payment_method->id() . ') created for user ' . $user->id() . ' (' . $user->getEmail() . ')');
      }
    }
    else {
      // Just log a message as this does not jeopardize the whole
      // order payment process.
      \Drupal::logger('commerce_easytransac')
        ->warning('Can not create/update user ' . $user->id() . ' payment method : ' . $card_response->getErrorMessage());
    }

    return $payment_method;
  }

  /**
   * {@inheritDoc}
   */
  public function isEligibleForMultiplePayments(OrderInterface $order): bool {
    $total = $order->getTotalPrice();
    if ($total === NULL) {
      return FALSE;
    }

    return $this->multiplePaymentEnabled()
      && EasyTransacHelper::isEligibleForMultiplePayments($total)
      && $order->getData('easytransac_payment_multiple', FALSE);
  }

  /**
   * {@inheritDoc}
   */
  public function getMultiplePaymentsCount(OrderInterface $order): ?int {
    if (!$this->isEligibleForMultiplePayments($order)) {
      return NULL;
    }

    $options = $this->multiplePaymentOptions();
    $default_option = current($options);
    return $order->getData('easytransac_payment_multiple_count', $default_option);
  }

  /**
   * {@inheritDoc}
   */
  public function multiplePaymentEnabled(): bool {
    return $this->configuration['multiple_payment'] ?? FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function multiplePaymentOptions(): array {
    return array_unique($this->configuration['multiple_payment_settings'] ?? []);
  }

  /**
   * {@inheritDoc}
   */
  public function oneClickPaymentEnabled(): bool {
    return $this->configuration['oneclick_payment'] ?? FALSE;
  }

}
