<?php

namespace Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway;

/**
 * Supports one click payment interface.
 *
 * Defines the interface for EasyTransac
 * gateways which support OneClick payments.
 */
interface SupportsOneClickPaymentInterface {

  /**
   * Check if EasyTransac oneClick payments are enabled.
   *
   * @return bool
   *   TRUE if oneClick payments are enabled, FALSE otherwise.
   */
  public function oneClickPaymentEnabled(): bool;

}
