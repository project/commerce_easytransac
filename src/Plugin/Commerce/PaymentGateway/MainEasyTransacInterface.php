<?php

namespace Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_easytransac\ApplicationInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use EasyTransac\Core\Services;
use EasyTransac\Entities\DoneTransaction;

/**
 * Defines the main interface for EasyTransac payment gateways.
 */
interface MainEasyTransacInterface extends OffsitePaymentGatewayInterface, SupportsAuthorizationsInterface, SupportsRefundsInterface {

  /**
   * Get the EasyTransac API key set for the payment gateway.
   *
   * @return string
   *   The EasyTransac API key.
   */
  public function getApiKey(): ?string;

  /**
   * Get the EasyTransac API environment.
   *
   * @return string
   *   Environment (production or test).
   */
  public function getEnvironment(): ?string;

  /**
   * Check if the EasyTransac API is in live mode.
   *
   * @return bool
   *   TRUE if live mode, FALSE otherwise (demo mode).
   */
  public function isLive(): bool;

  /**
   * Get the EasyTransac application for the payment gateway.
   *
   * @return \Drupal\commerce_easytransac\ApplicationInterface
   *   The EasyTransac application.
   */
  public function getApplication(): ApplicationInterface;

  /**
   * Get the EasyTransac API client.
   *
   * @return \EasyTransac\Core\Services
   *   The EasyTransac API client.
   */
  public function getClient(): Services;

  /**
   * Checks whether the given payment can be synced.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment to get status for.
   *
   * @return bool
   *   TRUE if the payment can be synced, FALSE otherwise.
   */
  public function canSyncPayment(PaymentInterface $payment): bool;

  /**
   * Synchronize a payment with the EasyTransac API.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment to synchronize.
   */
  public function syncPayment(PaymentInterface $payment): void;

  /**
   * Checks whether the given payment can be queried for its status.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment to get status for.
   *
   * @return bool
   *   TRUE if the payment can be queried for its status, FALSE otherwise.
   */
  public function canGetStatusOfPayment(PaymentInterface $payment): bool;

  /**
   * Get the status of a payment on EasyTransac API.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment to get status for.
   *
   * @return \EasyTransac\Entities\DoneTransaction
   *   EasyTransac API returned data.
   */
  public function getStatusOfPayment(PaymentInterface $payment): DoneTransaction;

}
