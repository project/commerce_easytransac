<?php

namespace Drupal\commerce_easytransac;

use Drupal\Core\Session\AccountInterface;
use EasyTransac\Core\Services;

/**
 * Provides the interface for the EasyTransac application.
 */
interface ApplicationInterface {

  /**
   * Get the EasyTransac API debug status for the payment gateway.
   *
   * @return bool
   *   The EasyTransac API debug status.
   */
  public function getDebug(): ?bool;

  /**
   * Get the EasyTransac API request timeout for the payment gateway.
   *
   * @return int|null
   *   The EasyTransac API request timeout, or NULL if none set.
   */
  public function getRequestTimeout(): ?int;

  /**
   * Get the EasyTransac API log path for the payment gateway.
   *
   * @return string|null
   *   The EasyTransac API log path, or NULL if none set.
   */
  public function getLogPath(): ?string;

  /**
   * Get the current user.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   Current user.
   */
  public function getCurrentUser(): AccountInterface;

  /**
   * Get the language to use for EasyTransac API.
   *
   * @return string
   *   Language code.
   */
  public function getLanguage(): string;

  /**
   * Prepare the EasyTransac API client.
   *
   * @param string $api_key
   *   The EasyTransac API key.
   *
   * @return \EasyTransac\Core\Services
   *   EasyTransac API client.
   */
  public function initClient(string $api_key): Services;

}
