<?php

namespace Drupal\commerce_easytransac;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use EasyTransac\Core\CurlCaller;
use EasyTransac\Core\Logger;
use EasyTransac\Core\Services;

/**
 * Represents the EasyTransac Application.
 */
class Application implements ApplicationInterface {

  /**
   * The application settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * API clients.
   *
   * @var array
   */
  protected array $clients = [];

  /**
   * Constructs a new Application object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountInterface $current_user, LanguageManagerInterface $language_manager) {
    $this->settings = $config_factory->get('commerce_easytransac.settings');
    $this->currentUser = $current_user;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getDebug(): ?bool {
    if (empty($this->settings->get('debug'))) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestTimeout(): ?int {
    if (!empty($this->settings->get('timeout'))) {
      return $this->settings->get('timeout');
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLogPath(): ?string {
    if (!empty($this->settings->get('log_path'))) {
      return $this->settings->get('log_path');
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function initClient(string $api_key): Services {
    $instance = Services::getInstance();
    $logger = Logger::getInstance();
    if ($instance === NULL || $logger === NULL) {
      throw new \RuntimeException('EasyTransac SDK not initialized.');
    }

    $instance->provideAPIKey($api_key);
    $instance->setDebug($this->getDebug());
    if (($timeout = $this->getRequestTimeout()) !== NULL) {
      $instance->setRequestTimeout($timeout);
    }

    if (($log_path = $this->getLogPath()) !== NULL) {
      $logger->setFilePath($log_path);
    }

    // If Drupal is configured to use a proxy for outgoing requests
    // make sure that the proxy CURLOPT_PROXY setting is passed to
    // the EasyTransac SDK client.
    $http_client_config = Settings::get('http_client_config');
    if (!empty($http_client_config['proxy']['https'])) {
      $caller = $instance->getCaller();
      if (!($caller instanceof CurlCaller)) {
        $logger->write('Caller is not a instance of CurlCaller, defined Drupal proxy might not be working.');
      }

      $headers = $caller->getHeaders();
      $headers[CURLOPT_PROXY] = $http_client_config['proxy']['https'];
      $caller->setHeaders($headers);
    }

    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getCurrentUser(): AccountInterface {
    return $this->currentUser;
  }

  /**
   * {@inheritDoc}
   */
  public function getLanguage(): string {
    return EasyTransac::formatLanguage($this->languageManager->getCurrentLanguage()->getId());
  }

}
