<?php

namespace Drupal\commerce_easytransac\PluginForm\EasyTransac;

use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * EasyTransac payment gateway plugin form to sync a payment.
 */
class PaymentSyncForm extends PaymentGatewayFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    $form['#theme'] = 'confirm_form';
    $form['#attributes']['class'][] = 'confirmation';
    $form['#page_title'] = $this->t('Are you sure you want to synchronize the %label payment with EasyTransac platform?', [
      '%label' => $payment->label(),
    ]);
    $form['#success_message'] = $this->t('Payment synchronized.');
    $form['description'] = [
      '#markup' => $this->t('This action cannot be undone.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway\EasyTransacAbstractGateway $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;
    $payment_gateway_plugin->syncPayment($payment);
  }

}
