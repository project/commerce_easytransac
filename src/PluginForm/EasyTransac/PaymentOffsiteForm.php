<?php

namespace Drupal\commerce_easytransac\PluginForm\EasyTransac;

use Drupal\commerce_easytransac\EasyTransac;
use Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway\EasyTransacInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\Form\FormStateInterface;
use EasyTransac\Entities\PaymentPageTransaction;
use EasyTransac\Requests\PaymentPage;

/**
 * Payment offsite form.
 *
 * Provides the off-site form for EasyTransac
 * checkout (regular checkout with credit card).
 */
class PaymentOffsiteForm extends AbstractOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    $user = $order->getCustomer();

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    if (!($payment_gateway_plugin instanceof EasyTransacInterface)) {
      throw new \UnexpectedValueException('Payment gateway must implement EasyTransacInterface.');
    }

    $minor_units_converter = \Drupal::service('commerce_price.minor_units_converter');
    $config = $payment_gateway_plugin->getConfiguration();

    $customer = \Drupal::service('commerce_easytransac.easytransac')
      ->getCustomer($order);

    // Define the existing EasyTransac client ID when possible.
    if (!empty($customer_remote_id = $payment_gateway_plugin->getRemoteCustomerId($user))) {
      $customer->setClientId($customer_remote_id);
    }

    $transaction = (new PaymentPageTransaction())
      ->setAmount($minor_units_converter->toMinorUnits($order->getTotalPrice()))
      ->setClientIP($order->getIpAddress())
      ->setDescription((string) $this->t('Payment of order #@id', ['@id' => $order->id()]))
      ->setPreAuth(FALSE)
      ->setSecure(TRUE)
      ->setSaveCard(TRUE)
      ->setCustomer($customer)
      ->setOrderId($order->id())
      ->setReturnUrl($form['#return_url'])
      ->setCancelUrl($form['#cancel_url'])
      ->setLanguage($payment_gateway_plugin->getApplication()->getLanguage());

    // Only authorization will be done, no capture yet.
    if ($form['#capture'] === FALSE) {
      $transaction
        ->setPreAuth(TRUE)
        ->setPreAuthDuration(!empty($config['authorization_duration']) ? $config['authorization_duration'] : EasyTransac::EASYTRANSAC_MIN_AUTH_DURATION);
    }

    // Multiple payments.
    if ($payment_gateway_plugin->isEligibleForMultiplePayments($order)) {
      $payments_count = $payment_gateway_plugin->getMultiplePaymentsCount($order);
      $transaction
        ->setMultiplePayments('yes')
        ->setMultiplePaymentsRepeat($payments_count)
        ->setDownPayment($minor_units_converter->toMinorUnits(EasyTransac::downPaymentAmount($order->getTotalPrice(), $payments_count)));
    }

    $payment_gateway_plugin->getClient();
    $response = (new PaymentPage())->execute($transaction);
    if (!$response->isSuccess()) {
      throw new PaymentGatewayException($response->getErrorMessage());
    }

    // Save the request identifier in the order for reference.
    /** @var \EasyTransac\Entities\PaymentPageInfos $payment_page_information */
    $payment_page_information = $response->getContent();
    $redirect_url = $payment_page_information->getPageUrl();
    $order->setData('easytransac_request_identifier', $payment_page_information->getRequestId());
    $order->save();

    return $this->buildRedirectForm($form, $form_state, $redirect_url, [], self::REDIRECT_GET);
  }

}
