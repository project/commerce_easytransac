<?php

namespace Drupal\commerce_easytransac\PluginForm\EasyTransac;

use Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway\PayByBankInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\Form\FormStateInterface;
use EasyTransac\Entities\PaymentPageTransaction;
use EasyTransac\Requests\PaymentPage;

/**
 * PayByBank offsite form.
 *
 * Provides the off-site form for EasyTransac
 * PayByBank checkout (specific process).
 */
class PayByBankOffsiteForm extends AbstractOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    if (!($payment_gateway_plugin instanceof PayByBankInterface)) {
      throw new \UnexpectedValueException('Payment gateway must implement PayByBankInterface.');
    }

    $customer = \Drupal::service('commerce_easytransac.easytransac')
      ->getCustomer($order);

    // Define the existing EasyTransac client ID when possible.
    if (!empty($customer_remote_id = $payment_gateway_plugin->getRemoteCustomerId($order->getCustomer()))) {
      $customer->setClientId($customer_remote_id);
    }

    $amount = \Drupal::service('commerce_price.minor_units_converter')
      ->toMinorUnits($order->getTotalPrice());

    $transaction = (new PaymentPageTransaction())
      ->setAmount($amount)
      ->setClientIP($order->getIpAddress())
      ->setDescription((string) $this->t('Payment of order #@id', ['@id' => $order->id()]))
      ->setCustomer($customer)
      ->setOrderId($order->id())
      ->setReturnUrl($form['#return_url'])
      ->setCancelUrl($form['#cancel_url'])
      ->setOperationType('paybybank')
      ->setLanguage($payment_gateway_plugin->getApplication()->getLanguage());

    $payment_gateway_plugin->getClient();
    $response = (new PaymentPage())->execute($transaction);
    if (!$response->isSuccess()) {
      throw new PaymentGatewayException($response->getErrorMessage());
    }

    // Save the request identifier in the order for reference.
    /** @var \EasyTransac\Entities\PaymentPageInfos $payment_page_information */
    $payment_page_information = $response->getContent();
    $redirect_url = $payment_page_information->getPageUrl();
    $order->setData('easytransac_request_identifier', $payment_page_information->getRequestId());
    $order->save();

    return $this->buildRedirectForm($form, $form_state, $redirect_url, [], self::REDIRECT_GET);
  }

}
