<?php

namespace Drupal\commerce_easytransac\PluginForm\EasyTransac;

use Drupal\commerce_easytransac\EasyTransac;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * EasyTransac payment gateway plugin form to get the status of a payment.
 */
class PaymentGetStatusForm extends PaymentGatewayFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway\PayByBankInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;

    $form['description'] = [
      '#markup' => $this->t('Status of the EasyTransac payment'),
    ];

    try {
      $items = [];
      $transactionContent = $payment_gateway_plugin->GetStatusOfPayment($payment);

      if (!empty($transactionContent->getStatus())) {
        $items[] = $this->t('Identifier : @identifier', [
          '@identifier' => $transactionContent->getTid(),
        ]);
      }

      if (!empty($transactionContent->getStatus())) {
        $status = $transactionContent->getStatus();
        // phpcs:ignore
        $translatedStatus = (string) $this->t($status);
        $items[] = $this->t('Status : @status (@raw)', [
          '@status' => $translatedStatus,
          '@raw' => $status,
        ]);
      }

      if (!empty($transactionContent->getMessage())) {
        $items[] = $this->t('Message : @message', [
          '@message' => $transactionContent->getMessage(),
        ]);
      }

      if (!empty($transactionContent->getError())) {
        $items[] = $this->t('Error : @error', [
          '@error' => $transactionContent->getError(),
        ]);
      }

      if (!empty($transactionContent->getAdditionalError())) {
        $items[] = $this->t('Additional error : @error', [
          '@error' => $transactionContent->getAdditionalError(),
        ]);
      }

      if (!empty($transactionContent->getRedirectUrl())) {
        $items[] = [
          '#markup' => $this->t('Redirect URL : <a href="@link">@link</a>', [
            '@link' => $transactionContent->getRedirectUrl(),
          ]),
        ];
      }

    }
    catch (PaymentGatewayException $exception) {
      $items[] = [
        '#markup' => $this->t('EasyTransac API responded : @error', ['@error' => $exception->getMessage()]),
      ];
    }

    if ($payment->hasField('request_id')
      && !$payment->get('request_id')->isEmpty()) {
      $pageUrl = EasyTransac::EASYTRANSAC_URL . '/pay/' . $payment->get('request_id')->first()->getValue()['value'];
      $items[] = [
        '#markup' => $this->t('Page URL : <a href="@link">@link</a>', ['@link' => $pageUrl]),
      ];
    }

    $form['status'] = [
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => '',
      '#list_type' => 'ul',
      '#attributes' => [],
    ];

    $form['easytransac_id'] = [
      '#type' => 'hidden',
      '#value' => 'easytransac_payment_get_status',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Nothing to do.
  }

}
