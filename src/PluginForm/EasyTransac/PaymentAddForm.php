<?php

namespace Drupal\commerce_easytransac\PluginForm\EasyTransac;

use Drupal\commerce_easytransac\EasyTransac;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\Core\Form\FormStateInterface;
use EasyTransac\Entities\OneClickTransaction;
use EasyTransac\Entities\PaymentPageTransaction;
use EasyTransac\Requests\OneClickPayment;
use EasyTransac\Requests\PaymentPage;

/**
 * Payment add form.
 *
 * EasyTransac payment gateway plugin form to add
 * a payment using a payment page.
 */
class PaymentAddForm extends PaymentGatewayFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    if (!$order) {
      throw new \InvalidArgumentException('Payment entity with no order reference given to PaymentAddForm.');
    }

    // The payment amount should not exceed the remaining order balance.
    $balance = $order->getBalance();
    if (!$balance) {
      throw new \InvalidArgumentException('The payment amount cannot be determined due to a NULL order balance.');
    }
    $amount = $balance->isPositive() ? $balance : $balance->multiply(0);

    $form['amount'] = [
      '#type' => 'commerce_price',
      '#title' => $this->t('Amount'),
      '#default_value' => $amount->toArray(),
      '#required' => TRUE,
    ];

    $form['capture'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Capture the transaction'),
      '#default_value' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $payment->amount = $values['amount'];

    /** @var \Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway\PayByBankInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;
    $payment_gateway_plugin_config = $payment_gateway_plugin->getConfiguration();

    $order = $payment->getOrder();
    $customer = \Drupal::service('commerce_easytransac.easytransac')
      ->getCustomer($order);

    // Define the existing EasyTransac client ID when possible.
    if (!empty($customer_remote_id = $payment_gateway_plugin->getRemoteCustomerId($order->getCustomer()))) {
      $customer->setClientId($customer_remote_id);
    }

    $amount = \Drupal::service('commerce_price.minor_units_converter')
      ->toMinorUnits($payment->getAmount());

    // Check if this is a OneClick payment, or a PaymentPageTransaction.
    $payment_method = $payment->getPaymentMethod();
    if (!empty($payment_method)) {
      // Using stored payment method, use a OneClickTransaction.
      $transaction = (new OneClickTransaction())
        ->setAmount($amount)
        ->setOrderId($order->id())
        ->setUid($order->getCustomerId())
        ->setClientIp($order->getIpAddress())
        ->setClientId($customer_remote_id)
        ->setAlias($payment_method->getRemoteId())
        ->setPreAuth(FALSE)
        ->setSecure(TRUE)
        ->setDescription((string) $this->t('Payment of order #@id', ['@id' => $order->id()]))
        ->setLanguage($payment_gateway_plugin->getApplication()->getLanguage());
    }
    else {
      // No payment method, use a PaymentPageTransaction.
      $transaction = (new PaymentPageTransaction())
        ->setAmount($amount)
        ->setOrderId($order->id())
        ->setClientIP($order->getIpAddress())
        ->setPreAuth(FALSE)
        ->setSecure(TRUE)
        ->setSaveCard(TRUE)
        ->setCustomer($customer)
        ->setDescription((string) $this->t('Payment of order #@id', ['@id' => $order->id()]))
        ->setLanguage($payment_gateway_plugin->getApplication()->getLanguage());
    }

    // Only authorization will be done, no capture yet.
    if (((bool) $values['capture']) === FALSE) {
      $transaction
        ->setPreAuth(TRUE)
        ->setPreAuthDuration(!empty($payment_gateway_plugin_config['authorization_duration']) ? $payment_gateway_plugin_config['authorization_duration'] : EasyTransac::EASYTRANSAC_MIN_AUTH_DURATION);
    }

    $payment_gateway_plugin->getClient();
    if (!empty($payment_method)) {
      $request = new OneClickPayment();
    }
    else {
      $request = new PaymentPage();
    }

    $response = $request->execute($transaction);
    if (!$response->isSuccess()) {
      throw new PaymentGatewayException($response->getErrorMessage());
    }

    if (!empty($payment_method)) {
      /** @var \EasyTransac\Entities\DoneTransaction $done_response */
      $done_response = $response->getContent();
      $payment->setState('new');
      $payment->setRemoteId($done_response->getTid());
      $payment->setRemoteState($done_response->getStatus());

      switch ($done_response->getStatus()) {
        case 'captured':
          $payment->setState('completed');
          $payment->save();
          break;

        case 'pending':
          $payment->setState('pending');
          $payment->save();
          break;

        case 'authorized':
          $payment->setState('authorization');
          $payment->save();
          break;

        case 'failed':
        default:
          $error = $done_response->getError();
          if (!empty($done_response->getAdditionalError())) {
            $error .= ' (' . implode(', ', $done_response->getAdditionalError()) . ')';
          }

          throw new DeclineException($error);
      }

    }
    else {
      /** @var \EasyTransac\Entities\PaymentPageInfos $payment_page_information */
      $payment_page_information = $response->getContent();
      $payment->setState('pending');
      $payment->setRemoteState($payment_page_information->getStatus());
      $payment->set('request_id', $payment_page_information->getRequestId());
      $payment->save();

      // Display page URL to the user.
      \Drupal::messenger()
        ->addMessage($this->t('Payment page created, proceed to payment using the following URL : <a href="@url">@url</a>', ['@url' => $payment_page_information->getPageUrl()]));
    }
  }

}
