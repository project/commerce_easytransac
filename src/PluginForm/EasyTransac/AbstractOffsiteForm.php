<?php

namespace Drupal\commerce_easytransac\PluginForm\EasyTransac;

use Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway\MainEasyTransacInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the abstract off-site form shared code for EasyTransac checkouts.
 */
abstract class AbstractOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    if (!($order instanceof OrderInterface)) {
      throw new \UnexpectedValueException('Can not get the order from the payment.');
    }

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    if (!($payment_gateway_plugin instanceof MainEasyTransacInterface)) {
      throw new \UnexpectedValueException('Payment gateway must implement MainEasyTransacInterface.');
    }

    return $form;
  }

}
