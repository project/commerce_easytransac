<?php

namespace Drupal\commerce_easytransac\PluginForm\EasyTransac;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\Core\Form\FormStateInterface;
use EasyTransac\Entities\PayByBank;
use EasyTransac\Requests\PayByBank as PayByBankRequest;

/**
 * EasyTransac payment gateway plugin form to add a payment using PayByBank.
 */
class PayByBankPaymentAddForm extends PaymentGatewayFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    if (!$order) {
      throw new \InvalidArgumentException('Payment entity with no order reference given to PaymentAddForm.');
    }

    // The payment amount should not exceed the remaining order balance.
    $balance = $order->getBalance();
    if (!$balance) {
      throw new \InvalidArgumentException('The payment amount cannot be determined due to a NULL order balance.');
    }
    $amount = $balance->isPositive() ? $balance : $balance->multiply(0);

    $form['amount'] = [
      '#type' => 'commerce_price',
      '#title' => $this->t('Amount'),
      '#default_value' => $amount->toArray(),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $payment->amount = $values['amount'];

    /** @var \Drupal\commerce_easytransac\Plugin\Commerce\PaymentGateway\PayByBankInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;

    $order = $payment->getOrder();
    $customer = \Drupal::service('commerce_easytransac.easytransac')
      ->getCustomer($order);

    // Define the existing EasyTransac client ID when possible.
    if (!empty($customer_remote_id = $payment_gateway_plugin->getRemoteCustomerId($order->getCustomer()))) {
      $customer->setClientId($customer_remote_id);
    }

    $amount = \Drupal::service('commerce_price.minor_units_converter')
      ->toMinorUnits($payment->getAmount());

    $transaction = (new PayByBank())
      ->setAmount($amount)
      ->setClientIP($order->getIpAddress())
      ->setDescription((string) $this->t('Payment for order #@id', ['@id' => $order->id()]))
      ->setCustomer($customer)
      ->setOrderId($order->id())
      ->setLanguage($payment_gateway_plugin->getApplication()->getLanguage());

    $payment_gateway_plugin->getClient();
    $response = (new PayByBankRequest())->execute($transaction);
    if (!$response->isSuccess()) {
      throw new PaymentGatewayException($response->getErrorMessage());
    }

    // Save the transaction identifier in the order for reference.
    /** @var \EasyTransac\Entities\DoneTransaction $transaction */
    $transaction = $response->getContent();
    $payment->setState('pending');
    $payment->setRemoteState($transaction->getStatus());
    $payment->setRemoteId($transaction->getTid());
    $payment->save();

    // Display page URL to the user.
    \Drupal::messenger()
      ->addMessage($this->t('Payment page created, proceed to payment using the following URL : <a href="@url">@url</a>', ['@url' => $transaction->getRedirectUrl()]));
  }

}
