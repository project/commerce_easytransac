<?php

namespace Drupal\commerce_easytransac;

/**
 * Environments available for EasyTransac API.
 */
class Environment {

  public const PRODUCTION = 'production';
  public const TEST = 'test';

}
