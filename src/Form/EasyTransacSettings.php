<?php

namespace Drupal\commerce_easytransac\Form;

use Drupal\commerce_easytransac\Application;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a configuration form for EasyTransac settings.
 */
class EasyTransacSettings extends ConfigFormBase {

  /**
   * The state store.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * The EasyTransac application.
   *
   * @var \Drupal\commerce_easytransac\Application
   */
  protected $application;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Constructs a new EasyTransacSettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\commerce_easytransac\Application $application
   *   The EasyTransac application.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Application $application, LanguageManagerInterface $language_manager, UrlGeneratorInterface $url_generator) {
    parent::__construct($config_factory);
    $this->application = $application;
    $this->languageManager = $language_manager;
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('commerce_easytransac.application'),
      $container->get('language_manager'),
      $container->get('url_generator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_easytransac.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_easytransac_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('commerce_easytransac.settings');

    $form['advanced'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Advanced options'),
    ];

    $max_execution_time = ini_get('max_execution_time');
    $form['advanced']['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Connexion timeout'),
      '#description' => $this->t('Connexion timeout (in seconds) applied to API calls.') . (!empty($max_execution_time) && $max_execution_time !== '-1' ? '<br/>' . $this->t('NB: your web server is limited to @nb seconds of execution.', ['@nb' => $max_execution_time]) : ''),
      '#default_value' => $config->get('timeout'),
      '#min' => 1,
      '#max' => $max_execution_time,
      '#step' => 1,
    ];

    $form['advanced']['debug_advanced'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Debug options'),
    ];

    $form['advanced']['debug_advanced']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug mode'),
      '#description' => $this->t('For testing purposes, enabling the debug mode will activate logs.'),
      '#default_value' => $config->get('debug'),
    ];

    $form['advanced']['debug_advanced']['log_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Log path'),
      '#description' => $this->t('Path to which the logs will be written. By default, the path is the running script path.'),
      '#default_value' => $config->get('log_path'),
      '#states' => [
        'visible' => [
          'input[name="debug"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('commerce_easytransac.settings');

    $config
      ->set('timeout', $form_state->getValue('timeout'))
      ->set('debug', $form_state->getValue('debug'))
      ->set('log_path', $form_state->getValue('log_path'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
