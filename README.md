INTRODUCTION
------------

The EasyTransac module adds a payment method to Drupal 8/9 Commerce in order to check out via EasyTransac's payment service.

REQUIREMENTS
------------

This module requires the following PHP specifications:

* cURL
* OpenSSL version >= 1.0.1

This module requires the following modules:

* Drupal commerce (https://www.drupal.org/project/commerce)

This module requires an EasyTransac account, please visit:

* EasyTransac (https://easytransac.com)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
  https://www.drupal.org/docs/extending-drupal/installing-modules
  for further information.


CONFIGURATION
-------------

* Create an account on https://www.easytransac.com and configure your
  application by allowing your server's IP address. This will provide you with
  an API Key. The notification URL can be found when configuring the EasyTransac payment gateway.

* Enter your API Key on the payment method configuration page, accessible via
  Commerce / Configuration / Payment / Payment gateways /
  Add a payment gateway : Easytransac / edit and don't forget
  to save the form.

* Make sure to set up HTTPS on your website, or you could face issues with the Cookies SameSite policy,
  and the user could be unlogged when coming back from the EasyTransac payment page.

FAQ
---

### Why do I get an error when I want to use authorize mode instead of capture ?

Your account might need some specific permissions to use the authorize mode.
Please contact us (https://www.easytransac.com/fr/nous-contacter) if you need this feature and experience such problems.

### Why do I get an error when I want to partially refund a payment ?

The partial refund can not be done on payments of the same day.
Payments done on the same day can only be voided completely as the transfers did not occur yet.
Some internal limitation of your EasyTransac account might forbid partial refunds as well,
contact us (https://www.easytransac.com/fr/nous-contacter) to get more information if you experience such problems.

### What is PayByBank ?

PayByBank is a service that allows your customer to log in their bank account and transfer money directly between banks.
This service must be enabled on your account in order to use it. To set up PayByBank, you have to configure the specific
"EasyTransac PayByBank" payment gateway in your Drupal website. Please note that only pre-authorization are supported,
the payment won't be validated after the user comes back to your shop and the payment will be updated automatically after
a few minutes. If the client is coming back to the shop, by pressing the "return" button on their browser, the order
won't be considered and cancelled because the platform does not support this feature. So, the order will be considered
completed but with a pending payment.

### What is the OneClick feature ?

The OneClick feature allows customers to keep their credit card stored securely in EasyTransac, as well as in the
Drupal backoffice. Only a card alias will be used to identify the object in the API, and the customer will only
see the last 4 digits, card type, and date of expiration in his dashboard.
On subsequent payments, the customer will be able to choose one of his saved credit card to proceed to payment faster.
Note: the next payments have to be lower or equal to the initial payment to work using the OneClick feature.

CREDITS
-------

* Sponsored by EasyTransac - https://www.easytransac.com
* Developped by SaaS Production - https://www.saas-production.com
